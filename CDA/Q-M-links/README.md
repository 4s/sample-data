
Questionnaire-Measurement link sample data (CDA)
================================================

The purpose of the sample data files in this folder is to demonstrate
the so-called "Questionnaire-Measurement link".

The questionnaire found in ´qfdd.xml´ includes a series of questions
about measurements performed by the patient: a weight and a blood
pressure measurement.

As a patient responds to this questionnaire, the resulting `qrd.xml`
and `phmr.xml` files are generated. In this particular case, the
patient uses a questionnaire-filling app which is capable of
recognizing the measurements and capturing the values automatically
from connected personal health devices. The patient in this case owns
a connected blood pressure meter, but does not have a connected weight
scale. Hence the weight measurement is typed into the questionnaire
form manually.


CDA sample files in this folder
===============================

Details about the mapping and modelling choices are found in the
inline comments of the `qfdd.xml`, `qrd.xml`, and `phmr.xml`files.


FHIR sample files in `../../FHIR-documented/Q-M-links`
======================================================

A FHIR-based representation of the exact same event can be found in
`../../FHIR-documented/Q-M-links`. The details about the mapping back
and forth between FHIR and CDA is also found in the inline comments of
the CDA sample files.


Important modelling remarks and unfinished business
===================================================

In order to highlight important remarks about the modelling choices
and questions / uncertainties for easy searching, the keyword 'NOTE'
is used throughout the comments of the CDA files (as well as the
FHIR files).

Unfinished business and temporary placeholders are marked in a similar
fashion using 'FIXME' and 'PLACEHOLDER' or 'placeholder' keywords.


OIDs used in these samples
==========================

The OIDs used for the Questionnaire-Measurement link samples are
summarized in the table below. All OIDs are allocated in the Central
Denmark Region test range (`1.2.208.180`) and otherwise not
particularly well-structured.


| OID                  | Type            | Description                                                       |
| -------------------- | --------------- | ----------------------------------------------------------------- |
| `1.2.208.180.42.1.1` | QFDD TemplateId | Questions Organizer subtype for automatic measurements            |
| `1.2.208.180.42.1.2` | QFDD TemplateId | PQ-Numeric or Text Question Pattern subtype for automatic capture |
| `1.2.208.180.42.1.3` | QRD TemplateId  | PQ-Numeric or Text Response Pattern subtype for automatic capture |
| `1.2.208.180.42.2.1` | Code system     | Questions Organizer code                                          |
| `1.2.208.180.42.2.2` | Code system     | Text Question Pattern Observation code                            |
| `1.2.208.180.42.3.1` | Code system     | Questionnaire code                                                |


`1.2.208.180.42.1.1`
--------------------

TemplateId marking a Questions Organizer, which may invoke an
automatic measurement.

 * SHALL conform to the Questions Organizer template
   (`2.16.840.1.113883.10.20.32.4.1`)
 * SHALL contain at least one
   participant/participantRole/playingDevice/code with the MDC code of
   the device profile (`MDC_DEV_(SUB_)SPEC_PROFILE_*`) of the Personal
   Health Device type(s) that can be used to collect the measurement.
 * SHALL contain at least one child observation conforming to
   `1.2.208.180.42.1.2`


`1.2.208.180.42.1.2`
--------------------

TemplateId marking a Numeric Question Pattern of the PQ type or a Text
Question Pattern, which may be completed automatically by the result
of an automatic measurement defined by the encompassing Questions
Organizer.

 * SHALL conform to either the Numeric Question Pattern Observation
   template (`2.16.840.1.11883.10.20.32.4.7`) or the Text Question
   Pattern template (`2.16.840.1.11883.10.20.32.4.9`). If it conforms
   to the Numeric Question Pattern Observation, this must have type
   `PQ` **which is currently not supported by the QFDD implementation
   guide**.
 * SHALL have a code (perhaps as a translation) from the MDC code
   system representing the feature to be captured. This can be a
   metric object (represented by its `MDC_PART_SCADA` code) or an
   attribute, including (but not restricted to) the following:
   * `MDC_ATTR_ID_MODEL` (Manufacturer + “ “ + model)
   * `MDC_ID_MODEL_MANUFACTURER` (Manufacturer)
   * `MDC_ID_MODEL_NUMBER` (Model)
   * `MDC_ATTR_SYS_ID` (EUI-64 device id)
   * `MDC_ID_PROD_SPEC_SERIAL` (Serial number)


`1.2.208.180.42.1.3`
--------------------

TemplateId marking a Numeric Response Pattern of the PQ type or a Text
Response Pattern, which represents a measurement, and may have been
completed automatically.

 * SHALL conform to either the Numeric Response Pattern Observation
   template (`2.16.840.1.11883.10.20.33.4.4`) or the Text Response
   Pattern template (`2.16.840.1.11883.10.20.33.4.6`). If it conforms
   to the Numeric Response Pattern Observation, this must have type
   `PQ` **which is currently not supported by the QRD implementation
   guide**.
 * SHALL contain a `methodCode` with a suitable code picked from the
   "MedCom Message Codes" system `1.2.208.184.100.1` indicating
   whether the measurement was typed into the questionnaire form or
   captured automatically from some PHD equipment.
 * SHALL contain a `reference` of `typeCode="ELNK"` with a "deep"
   reference directly to the corresponding observation inside a PHMR
   document. This reference must comply with the MedCom profile
   "Eksterne referencer i CDA dokumenter"


`1.2.208.180.42.2.1`
-------------------

Questions Organizer type code system. Defined codes: “Weight”, “BP” 


`1.2.208.180.42.2.2`
--------------------

Text Question Pattern Observation code system. Defined codes: “ModelNumber”


`1.2.208.180.42.3.1`
--------------------

Questionnaire code system. Defined codes: "75" (Weight and blood pressure questionnaire) 
