/*
 * This file is a continuation of the example "Continua-inspired"
 * CTG online streaming. See 1.jsonc for more info
 *
 * @author Jacob Andersen
 */



{
  "resourceType": "Observation",
  "meta": {
    "profile": [
      "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/115146755/PhdScannerPanelObservation"
    ]
  },
  "identifier": [
    {
      "value": "0201919996-urn:oid:1.2.208.176.1.2-00-80-98-FE-FF-0E-39-13-8450059-250.0-8-323-20170612T104811.00"
    }
  ],
  "status": "preliminary",
  "code": {
    "coding": [
      {
        "system": "urn:iso:std:iso:11073:10101",
        "code": "8450059",
        "display": "MDC_PLACEHOLDER_RECORDING_PANEL_SELF_PERFORMED_CTG"
      }
    ],
    "text": "MDC_PLACEHOLDER_RECORDING_PANEL_SELF_PERFORMED_CTG: CTG recording"
  },
  "subject": {
    "identifier": {
      "system": "urn:oid:1.2.208.176.1.2",
      "value": "0201919996" // Ellen Louise Test Lauridsen
    },
    "type": "Patient"
  },
  
  /* 
   * Note: performer is not a required element. In a
   * telemonitoring scenario, if a perfomer is not included, it
   * is implied that the subject is also the performer.
   */

  "performer": [
    {
      "identifier": {
        "system": "urn:oid:1.2.208.176.1.2",
        "value": "0201919996" // Ellen Louise Test Lauridsen
      },
      "type": "Patient"
    }
  ],
  "effectivePeriod": {
    "start": "2017-06-12T10:48:11+02:00",
    "end": "2017-06-12T10:48:13+02:00"
  },
  "device": {
    "identifier": {
              "system": "urn:oid:1.2.840.10004.1.1.1.0.0.1.0.0.1.2680",
              "value": "00-80-98-FE-FF-0E-39-13",
              "assigner": {"display": "EUI-64"}
          }
  },
  "hasMember": [
    {
      "reference": "#childObs1" // Maternal heart rate
    },
    {
      "reference": "#childObs2" // Fetal heart rate
    },
    {
      "reference": "#childObs3" // Uterine activity
    },
    {
      "reference": "#childObs4" // Signal quality
    },
    {
      "reference": "#childObs5" // Patient status observation
    }
  ],
  "extension": [
    {
      /*
       * This Observation is the continuation of a previous recording
       */

      "url": "http://hl7.org/fhir/StructureDefinition/observation-sequelTo",
      "valueReference": {
        "identifier": {
          "value": "0201919996-urn:oid:1.2.208.176.1.2-00-80-98-FE-FF-0E-39-13-8450059-250.0-8-322-20170612T104809.00"
        }
      }
    }, {
      /*
       * The PHG device collecting this measurement
       */
      
      "url": "http://hl7.org/fhir/StructureDefinition/observation-gatewayDevice",
      "valueReference": {
        "identifier": {
              "system": "urn:oid:1.2.840.10004.1.1.1.0.0.1.0.0.1.2680",
              "value": "7E-ED-AB-EE-34-AD-BE-EF",
              "assigner": {"display": "EUI-64"}
          }
      }
      
    }, {
      /*
       * The measurement was performed automatically
       */
      
      "url": "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/114262030/Observation+dataEntryMethod",
      "valueCoding": {
        "code": "automatic",
        "system": "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100728837/ObservationDefinition+dataEntryMethod+coding+system",
        "display": "Automatisk måling"
      }
    }, {
      /*
       * A stream ID identifying this stream of CTG data
       */

      "url": "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/115474442/Observation+streamID",
      "valueUri" : "urn:uuid:d4bcbc32-e9e9-4310-9a30-251f2aaee389"
    }
  ],

  
  /*
   * The embedded child observations
   */
  
  "contained": [

    
    /*
     * Maternal Heart Rate Observation resource
     */
    
    {
      "resourceType": "Observation",
      "id": "childObs1",
      "meta": {
        "profile": [
          "http://hl7.org/fhir/uv/phd/StructureDefinition/PhdRtsaObservation"
        ]
      },
      "status": "preliminary",
      "code": {
        "coding": [
          {
            "system": "urn:iso:std:iso:11073:10101",
            "code": "149546",
            "display": "MDC_PULS_RATE_NON_INV"
          },
          {
            "system": "http://loinc.org",
            "code": "8867-4",
            "display": "Heart Rate"
          }
        ],
        "text": "MDC_PULS_RATE_NON_INV: Mother's heart rate"
      },
      "subject": {
        "identifier": {
          "system": "urn:oid:1.2.208.176.1.2",
          "value": "0201919996" // Ellen Louise Test Lauridsen
        },
        "type": "Patient"
      },
      "effectivePeriod": {
        "start": "2017-06-12T10:48:11+02:00",
        "end": "2017-06-12T10:48:13+02:00"
      },
      "device": {
        "identifier": {
              "system": "urn:oid:1.2.840.10004.1.1.1.0.0.1.0.0.1.2680",
              "value": "00-80-98-FE-FF-0E-39-13",
              "assigner": {"display": "EUI-64"}
          }
      },
      "valueSampledData": {
        "origin": {
          "value": 0.0,
          "unit": "bpm",
          "system": "http://unitsofmeasure.org",
          "code": "/min"
        },
        "period": 250.0,
        "factor": 0.25,
        "dimensions": 1,
        "data": "323 323 322 322 323 324 323 322"
      },
      "referenceRange": [{
        "low": {
          "value": 0.25,
          "system": "http://unitsofmeasure.org",
          "code": "/min"
        },
        "high": {
          "value": 300.0,
          "system": "http://unitsofmeasure.org",
          "code": "/min"
        }
      }],
      "component": [
        {
          "code": {
            "coding": [
              {
                "system": "urn:iso:std:iso:11073:10101",
                "code": "68193",
                "display": "MDC_ATTR_SUPPLEMENTAL_TYPES"
              }
            ]
          },
          "valueCodeableConcept": {
            "coding": [
              {
                "system": "urn:iso:std:iso:11073:10101",
                "code": "8450060",
                "display": "MDC_CTG_PLACEHOLDER_SOURCE_ECG"
              }
            ]
          }
        }
      ],
      "extension": [
        {          
          /*
           * The index of the first item in the valueSampleData
           * array in the overall recording
           */
          
          "url": "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/115474450/Observation+streamSampleIndex",
          "valueUnsignedInt": 24
        }
      ]
    },


    /*
     * Fetal Heart Rate Observation resource
     */
    
    {
      "resourceType": "Observation",
      "id": "childObs2",
      "meta": {
        "profile": [
          "http://hl7.org/fhir/uv/phd/StructureDefinition/PhdRtsaObservation"
        ]
      },
      "status": "preliminary",
      "code": {
        "coding": [
          {
            "system": "urn:iso:std:iso:11073:10101",
            "code": "8450048",
            "display": "MDC_CTG_PLACEHOLDER_FETAL_HEART_RATE_INSTANT"
          }
        ],
        "text": "MDC_CTG_PLACEHOLDER_FETAL_HEART_RATE_INSTANT: Fetal heart rate"
      },
      "subject": {
        "identifier": {
          "system": "urn:oid:1.2.208.176.1.2",
          "value": "0201919996" // Ellen Louise Test Lauridsen
        },
        "type": "Patient"
      },
      "effectivePeriod": {
        "start": "2017-06-12T10:48:11+02:00",
        "end": "2017-06-12T10:48:13+02:00"
      },
      "device": {
        "identifier": {
              "system": "urn:oid:1.2.840.10004.1.1.1.0.0.1.0.0.1.2680",
              "value": "00-80-98-FE-FF-0E-39-13",
              "assigner": {"display": "EUI-64"}
          }
      },
      "valueSampledData": {
        "origin": {
          "value": 0.0,
          "unit": "bpm",
          "system": "http://unitsofmeasure.org",
          "code": "/min"
        },
        "period": 250.0,
        "factor": 0.25,
        "dimensions": 1,
        "data": "560 561 562 563 562 561 561 562"
      },
      "referenceRange": [{
        "low": {
          "value": 0.25,
          "system": "http://unitsofmeasure.org",
          "code": "/min"
        },
        "high": {
          "value": 300.0,
          "system": "http://unitsofmeasure.org",
          "code": "/min"
        }
      }],
      "component": [
        {
          "code": {
            "coding": [
              {
                "system": "urn:iso:std:iso:11073:10101",
                "code": "68193",
                "display": "MDC_ATTR_SUPPLEMENTAL_TYPES"
              }
            ]                
          },
          "valueCodeableConcept": {
            "coding": [
              {
                "system": "urn:iso:std:iso:11073:10101",
                "code": "8450060",
                "display": "MDC_CTG_PLACEHOLDER_SOURCE_ECG"
              }
            ]
          }
        }
      ],
      "extension": [
        {          
          /*
           * The index of the first item in the valueSampleData
           * array in the overall recording
           */
          
          "url": "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/115474450/Observation+streamSampleIndex",
          "valueUnsignedInt": 24
        }
      ]
    },


    /*
     * Uterine Activity Observation resource
     */
    
    {
      "resourceType": "Observation",
      "id": "childObs3",
      "meta": {
        "profile": [
          "http://hl7.org/fhir/uv/phd/StructureDefinition/PhdRtsaObservation"
        ]
      },
      "status": "preliminary",
      "code": {
        "coding": [
          {
            "system": "urn:iso:std:iso:11073:10101",
            "code": "8450049",
            "display": "MDC_CTG_PLACEHOLDER_UTERINE_ACTIVITY"
          }
        ],
        "text": "MDC_CTG_PLACEHOLDER_UTERINE_ACTIVITY: Uterine activity"
      },
      "subject": {
        "identifier": {
          "system": "urn:oid:1.2.208.176.1.2",
          "value": "0201919996" // Ellen Louise Test Lauridsen
        },
        "type": "Patient"
      },
      "effectivePeriod": {
        "start": "2017-06-12T10:48:11+02:00",
        "end": "2017-06-12T10:48:13+02:00"
      },
      "device": {
        "identifier": {
              "system": "urn:oid:1.2.840.10004.1.1.1.0.0.1.0.0.1.2680",
              "value": "00-80-98-FE-FF-0E-39-13",
              "assigner": {"display": "EUI-64"}
          }
      },
      "valueSampledData": {
        "origin": {
          "value": 0.0,
          "unit": "µV",
          "system": "http://unitsofmeasure.org",
          "code": "uV"
        },
        "period": 250.0,
        "factor": 1.96,
        "dimensions": 1,
        "data": "41 40 41 43 42 42 40 41"
      },
      "referenceRange": [{
        "low": {
          "value": 0,
          "system": "http://unitsofmeasure.org",
          "code": "uV"
        },
        "high": {
          "value": 500,
          "system": "http://unitsofmeasure.org",
          "code": "uV"
        }
      }],
      "component": [
        {
          "code": {
            "coding": [
              {
                "system": "urn:iso:std:iso:11073:10101",
                "code": "68193",
                "display": "MDC_ATTR_SUPPLEMENTAL_TYPES"
              }
            ]
          },
          "valueCodeableConcept": {
            "coding": [
              {
                "system": "urn:iso:std:iso:11073:10101",
                "code": "8450062",
                "display": "MDC_CTG_PLACEHOLDER_SOURCE_EHG"
              }
            ]
          }
        }
      ],
      "extension": [
        {          
          /*
           * The index of the first item in the valueSampleData
           * array in the overall recording
           */
          
          "url": "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/115474450/Observation+streamSampleIndex",
          "valueUnsignedInt": 24
        }
      ]
    },


    /*
     * Signal Quality Observation resource
     */
    
    {
      "resourceType": "Observation",
      "id": "childObs4",
      "meta": {
        "profile": [
          "http://hl7.org/fhir/uv/phd/StructureDefinition/PhdRtsaObservation"
        ]
      },
      "status": "preliminary",
      "code": {
        "coding": [
          {
            "system": "urn:iso:std:iso:11073:10101",
            "code": "8450053",
            "display": "MDC_CTG_PLACEHOLDER_SIGNAL_QUALITY"
          }
        ],
        "text": "MDC_CTG_PLACEHOLDER_SIGNAL_QUALITY: Signal quality"
      },
      "subject": {
        "identifier": {
          "system": "urn:oid:1.2.208.176.1.2",
          "value": "0201919996" // Ellen Louise Test Lauridsen
        },
        "type": "Patient"
      },
      "effectivePeriod": {
        "start": "2017-06-12T10:48:11+02:00",
        "end": "2017-06-12T10:48:13+02:00"
      },
      "device": {
        "identifier": {
              "system": "urn:oid:1.2.840.10004.1.1.1.0.0.1.0.0.1.2680",
              "value": "00-80-98-FE-FF-0E-39-13",
              "assigner": {"display": "EUI-64"}
          }
      },
      "valueSampledData": {
        "origin": {
          "value": 0.0,
          "unit": "1",
          "system": "http://unitsofmeasure.org",
          "code": "1"
        },
        "period": 250.0,
        "factor": 1.00,
        "dimensions": 1,
        "data": "2 2 2 1 2 2 1 2"
      },
      "referenceRange": [{
        "low": {
          "value": 0,
          "system": "http://unitsofmeasure.org",
          "code": "1"
        },
        "high": {
          "value": 2,
          "system": "http://unitsofmeasure.org",
          "code": "1"
        }
      }],
      "extension": [
        {          
          /*
           * The index of the first item in the valueSampleData
           * array in the overall recording
           */
          
          "url": "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/115474450/Observation+streamSampleIndex",
          "valueUnsignedInt": 24
        }
      ]
    },


    /*
     * Patient Status Observation resource
     */
    
    {
      "resourceType": "Observation",
      "id": "childObs5",
      "meta": {
        "profile": [
          "http://hl7.org/fhir/uv/phd/StructureDefinition/PhdCodedEnumerationObservation"
        ]
      },
      "status": "preliminary",
      "code": {
        "coding": [
          {
            "system": "urn:iso:std:iso:11073:10101",
            "code": "8450054",
            "display": "MDC_CTG_PLACEHOLDER_PATIENT_STATUS"
          }
        ],
        "text": "MDC_CTG_PLACEHOLDER_PATIENT_STATUS: Patient status"
      },
      "subject": {
        "identifier": {
          "system": "urn:oid:1.2.208.176.1.2",
          "value": "0201919996" // Ellen Louise Test Lauridsen
        },
        "type": "Patient"
      },
      "effectivePeriod": {
        "start": "2017-06-12T10:48:11+02:00",
        "end": "2017-06-12T10:48:13+02:00"
      },
      "device": {
        "identifier": {
              "system": "urn:oid:1.2.840.10004.1.1.1.0.0.1.0.0.1.2680",
              "value": "00-80-98-FE-FF-0E-39-13",
              "assigner": {"display": "EUI-64"}
          }
      },
      "valueCodeableConcept": {
        "coding": [
          {
            "system": "urn:iso:std:iso:11073:10101",
            "code": "8450055",
            "display": "MDC_CTG_PLACEHOLDER_PATIENT_STATUS_ANTENATAL"
          }
        ]
      }
    }
  ]
}
