
Questionnaire-Measurement link sample data (FHIR)
=================================================

The purpose of the sample data files in this folder is to demonstrate
the so-called "Questionnaire-Measurement link".

The questionnaire found in `Questionnaire.jsonc` includes a series of
questions about measurements performed by the patient: a weight and a
blood pressure measurement.

As a patient responds to this questionnaire, the resulting
`Bundle.jsonc` is generated. In this particular case, the patient uses a
questionnaire-filling app which is capable of recognizing the
measurements and capturing the values automatically from connected
personal health devices. The patient in this case owns a connected
blood pressure meter, but does not have a connected weight
scale. Hence the weight measurement is typed into the questionnaire
form manually.


FHIR sample files in this folder
================================

Details about the mapping and modelling choices are found in the
inline comments of the `Questionnaire.jsonc` and `Bundle.jsonc`files.


CDA sample files in `../../CDA/Q-M-links`
=========================================

A CDA-based representation of the exact same event can be found in
`../../CDA/Q-M-links`. The details about the mapping back and forth
between FHIR and CDA is also found in the inline comments of the CDA
sample files.


Important modelling remarks and unfinished business
===================================================

In order to highlight important remarks about the modelling choices
and questions / uncertainties for easy searching, the keyword 'NOTE'
is used throughout the comments of the `.jsonc` files (as well as the
CDA files).

Unfinished business and temporary placeholders are marked in a similar
fashion using 'FIXME' and 'PLACEHOLDER' or 'placeholder' keywords.
