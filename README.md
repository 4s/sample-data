
Sample Data
===========

The overall purpose of this repository is to collect message and
document samples used for the documentation and test of interfaces
between different 4S modules in a single place. This is to avoid
having to maintain (and synchronise) interface test data across
multiple modules.

The structure of the test data folders is experimental! Currently the
folder structure partitions the sample data according to the data
type. This may change in the future. If you have a better idea on how
to organize this stuff, please do tell.


Current Folder Structure
========================

The top-level folders for documented interfaces are `CDA` and
`FHIR-documented`. These folders are subdivided by different topics.

The third `FHIR-clean` folder contains FHIR resources for testing
purposes.


Comments in FHIR (JSON)
=======================

In order to document the FHIR interfaces properly, JSON files
including documentation is kept in the `FHIR-documented` folder. The
suffix of these files is `.jsonc` (JSON with comments).

The `.jsonc` inline documentation uses standard C/C++/Java type
comments, i.e. `/* */` and `//`.

As comments are normally forbidden in pure JSON files, they must be
purged before the files can be used as test data. This can be
accomplished using the following filter:

```bash
cd src/main/resources/FHIR-documented
for f in $(find . -name *.jsonc) ; do \
  f=$(echo $f | cut -f 2 -d ".") ; \
  in=".${f}.jsonc" \
  out="../FHIR-clean/${f}.json" ; \
  cat $in | cpp -xc++ -P - | sed '/^[[:space:]]*$/d' > $out ; \
done
```

Clean copies of all the documented JSON files in `FHIR-documented`
should be kept in the `FHIR-clean` folder (along with other test
resources) ready for testing. Whenever the contents of a `.jsonc` file
is changed in `FHIR-documented`, the corresponding file in
`FHIR-clean` should also be updated according to the method above.

FHIR validator
==============

The HL7 FHIR validator can be used like this:
```
java -jar org.hl7.fhir.validator.jar <ressource>.json -version 4.0
```