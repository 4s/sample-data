/*
 * This file shows and explains an example "Continua-inspired"
 * combined questionnaire response and measurement FHIR upload using
 * a reporting-type (bundle) upload.
 * 
 * Note that the example is based on the December 2017 draft Continua
 * implementation guideline H.812.5 amended by the latest changes
 * hosted at http://build.fhir.org/ig/HL7/PHD/index.html
 *
 * The purpose of this example is to demonstrate the so-called
 * "Questionnaire-Measurement link". The content of this Bundle is
 * supposed to be the result of a patient responding to the
 * questionnaire found in the Questionnaire.jsonc file. Answering this
 * questionnaire (on an application supporting this use case) will
 * result in the creation of a number of FHIR resources, all reported
 * together in this Bundle:
 *
 *   - QuestionnaireResponse: The response to the Questionnaire
 *   - Observation(s):  The measurement(s) performed as part of
 *                      responding to the questionnaire
 *   - Device [PHG]:    The app used to respond to the questionnaire
 *                      and collect the measurements
 *   - Device(s) [PHD]: The PHD devices used for automatic collection
 *                      of measurements.
 *   - Patient:         The subject of the response/measurements
 * 
 * The content of this FHIR Bundle is supposed to be essentially
 * equivalent to the QRD and PHMR example documents contained in
 * qrd.xml and phmr.xml found among the CDA samples, and these
 * documents also contain notes about how this mapping should be
 * realized.
 *
 * For this scenario, we assume that a patient was asked to respond to
 * the questionnaire defined in Questionnaire.jsonc. This
 * questionnaire asks for two measurements, weight and blood
 * pressure. The patient owns a Continua compliant blood pressure
 * device (A&D UA-767PBT-C) but just a plain old analog bathroom
 * weight scale. Therefore, only the blood pressure related questions
 * could be completed automatically - the weight was typed into the
 * questionnaire form manually.
 *
 * @author Jacob Andersen
 */




{
  "resourceType": "Bundle",
  "type": "transaction",
  "meta": {
    "lastUpdated": "2019-06-28T10:00:00.000Z",
    "fhir_comments": [
      "Example constructed by Jacob Andersen",
      "Generated by hand :-)"
    ]
  },
  "entry": [
    
    /*
     * The PHG
     */
    
    {
      "request": {
        "method": "PUT",
        "url": "Device/7EEDABEE34ADBEEF.C4B301D280C6"
      },
      "fullUrl": "7EEDABEE34ADBEEF.C4B301D280C6",
      "resource": {
        "resourceType": "Device",
        "id": "7EEDABEE34ADBEEF.C4B301D280C6",
        "meta": {
          "profile": [
            "http://hl7.org/fhir/uv/phd/StructureDefinition/PhgDevice"
          ]
        },
        "identifier": [
          {
            "type": {
              "coding": [{
                "system": "http://hl7.org/fhir/uv/phd/CodeSystem/ContinuaDeviceIdentifiers",
                "code": "SYSID"
              }]
            },
            "system": "urn:oid:1.2.840.10004.1.1.1.0.0.1.0.0.1.2680",
            "value": "7E-ED-AB-EE-34-AD-BE-EF"
          },{
            "type": {
              "coding": [{
                "system": "http://hl7.org/fhir/uv/phd/CodeSystem/ContinuaDeviceIdentifiers",
                "code": "ETHMAC"
              }]
            },
            "system": "http://hl7.org/fhir/sid/eui-48",
            "value": "C4-B3-01-D2-80-C6"
          }
        ],
        "type": {
          "coding": [
            {
              "system": "urn:iso:std:iso:11073:10101",
              "code": "531981",
              "display": "MDC_MOC_VMS_MDS_AHD"
            }
          ],
          "text": "Continua Personal Health Gateway"
        },
        "manufacturer": "Alexandra Instituttet A/S",
        "modelNumber": "4S PHG Proof-of-Concept",
        "version": [
          {
            "type": {
              "coding": [
                {
                  "system": "urn:iso:std:iso:11073:10101",
                  "code": "531975",
                  "display": "MDC_ID_PROD_SPEC_SW"
                }
              ],
              "text": "Software revision"
            },
            "value": "0.1"
          },
          {
            "type": {
              "coding": [
                {
                  "system": "urn:iso:std:iso:11073:10101",
                  "code": "532352",
                  "display": "MDC_REG_CERT_DATA_CONTINUA_VERSION"
                }
              ],
              "text": "Continua Design Guidelines version"
            },
            "value": "07.00"
          }
        ],
        "property": [
          {
            "type": {
              "coding": [
                {
                  "system": "urn:iso:std:iso:11073:10101",
                  "code": "532353",
                  "display": "MDC_REG_CERT_DATA_CONTINUA_CERT_DEV_LIST"
                }
              ],
              "text": "Continua Device interface certification statement"
            },
            "valueCode": [
              {
                "coding": [
                  {
                    "system": "http://hl7.org/fhir/uv/phd/CodeSystem/ContinuaPHD",
                    "code": "16457",
                    "display": "BT HDP Generic"
                  }
                ],
                "text": "Continua certified devices list: BT HDP Generic"
              }
            ]
          },{
            "type": {
              "coding": [
                {
                  "system": "urn:iso:std:iso:11073:10101",
                  "code": "532355",
                  "display": "MDC_REG_CERT_DATA_CONTINUA_AHD_CERT_LIST"
                }
              ],
              "text": "Continua services interface certification statement"
            },
            "valueCode": [
              {
                "coding": [
                  {
                    "system": "http://hl7.org/fhir/uv/phd/CodeSystem/ContinuaHFS",
                    "code": "7",
                    "display": "observation-upload-fhir"
                  }
                ],
                "text": "Continua certified services list: H.812.5 FHIR Observation Upload"
              }
            ]
          },
          {
            "type": {
              "coding": [
                {
                  "system": "http://hl7.org/fhir/uv/phd/CodeSystem/ASN1ToHL7",
                  "code": "532354.0",
                  "display": "regulation-status"
                }
              ],
              "text": "Regulation status"
            },
            "valueCode": [
              {
                "coding": [
                  {
                    "system": "http://terminology.hl7.org/CodeSystem/v2-0136",
                    "code": "N",
                    "display": "regulated"
                  }
                ],
                "text": "Regulated medical device"
              }
            ]
          },
          {
            "type": {
              "coding": [
                {
                  "system": "urn:iso:std:iso:11073:10101",
                  "code": "68220",
                  "display": "MDC_TIME_SYNC_PROTOCOL"
                }
              ],
              "text": "Time synchronization"
            },
            "valueCode": [
              {
                "coding": [
                  {
                    "system": "urn:iso:std:iso:11073:10101",
                    "code": "532225",
                    "display": "MDC_TIME_SYNC_NTPV3"
                  }
                ],
                "text": "NTPv3"
              }
            ]
          }
        ]
      }
    },
    
    
    
    /*
     * The PHD
     *
     * In this example there are two measurements defined. The first
     * is a weight measurement and the second is a blood pressure
     * measurement. Only the blood pressure measurement results in the
     * creation of a PHD Device resource, as this is the only
     * automatic measurement.
     *
     * Notice that in this example we skip all the time sync / coincident
     * time stamp stuff
     */
    {
      "request": {
        "method": "PUT",
        "url": "Device/00091FFEFF801A33.00091F801A33"
      },
      "fullUrl": "00091FFEFF801A33.00091F801A33",
      "resource": {
        "resourceType": "Device",
        /* Notice that the id is defined by Continua */
        "id": "00091FFEFF801A33.00091F801A33",
        "meta": {
          "profile": [
            "http://hl7.org/fhir/uv/phd/StructureDefinition/PhdDevice",
            "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/116064297/Device+LinkedMeasurements"
          ]
        },
        "identifier": [
          {
            "type": {
              "coding": [{
                "system": "http://hl7.org/fhir/uv/phd/CodeSystem/ContinuaDeviceIdentifiers",
                "code": "SYSID"
              }]
            },
            "system": "urn:oid:1.2.840.10004.1.1.1.0.0.1.0.0.1.2680",
            "value": "00-09-1F-FE-FF-80-1A-33"
          }
        ],
        "type": {
          "coding": [
            {
              "system": "urn:iso:std:iso:11073:10101",
              "code": "65573",
              "display": "MDC_MOC_VMS_MDS_SIMP"
            }
          ],
          "text": "MDC_MOC_VMS_MDS_SIMP: Continua Personal Health Device"
        },
        "specialization": [
          {
            "systemType": {
              "coding": [
                {
                  "system": "urn:iso:std:iso:11073:10101",
                  "code": "528391",
                  "display": "MDC_DEV_SPEC_PROFILE_BP"
                }
              ],
              "text": "MDC_DEV_SPEC_PROFILE_BP: Blood pressure meter"
            },
            "version": "1"
          }
        ],
        "manufacturer": "A&D Medical",
        /*
         * The Model number is referenced from one of the questions,
         * so we insert a local id as anchor for the direct reference.
         */
        "modelNumber": "UA-767PBT-C",
        "_modelNumber": {"id": "anchor1"},
        "serialNumber": "5100100417",
        "version": [
          {
            "type": {
              "coding": [
                {
                  "system": "urn:iso:std:iso:11073:10101",
                  "code": "532352",
                  "display": "MDC_REG_CERT_DATA_CONTINUA_VERSION"
                }
              ],
              "text": "Continua Design Guidelines version"
            },
            "value": "02.00"
          }
        ],
        "property": [
          {
            "type": {
              "coding": [
                {
                  "system": "urn:iso:std:iso:11073:10101",
                  "code": "532353",
                  "display": "MDC_REG_CERT_DATA_CONTINUA_CERT_DEV_LIST"
                }
              ],
              "text": "Continua Device interface certification statement"
            },
            "valueCode": [
              {
                "coding": [
                  {
                    "system": "http://hl7.org/fhir/uv/phd/CodeSystem/ContinuaPHD",
                    "code": "16391",
                    "display": "BT HDP Blood Pressure Meter"
                  }
                ],
                "text": "Continua certified devices list: BT HDP Blood Pressure Meter"
              }
            ]
          },
          {
            "type": {
              "coding": [
                {
                  "system": "http://hl7.org/fhir/uv/phd/CodeSystem/ASN1ToHL7",
                  "code": "532354.0",
                  "display": "regulation-status"
                }
              ],
              "text": "Regulation status"
            },
            "valueCode": [
              {
                "coding": [
                  {
                    "system": "http://terminology.hl7.org/CodeSystem/v2-0136",
                    "code": "N",
                    "display": "regulated"
                  }
                ],
                "text": "Regulated medical device"
              }
            ]
          }
        ]
      }
    },
    
    
    /*
     * Patient resource
     */
    
    {
      "request": {
        "method": "POST",
        "url": "Patient",
        "ifNoneExist": "identifier=urn:oid:1.2.208.176.1.2|2512489996"
      },
      "fullUrl": "urn:uuid:d474d2a8-9f94-4836-9094-01108864af54",
      "resource": {
        "resourceType": "Patient",
        "meta": {
          "profile": [
            "http://hl7.org/fhir/uv/phd/StructureDefinition/PhdPatient"
          ]
        },
        "identifier": [
          {
            // We use the CPR number as identifier in this example,
            // but it could be any unique identification of the
            // patient.
            "system": "urn:oid:1.2.208.176.1.2",
            "value": "2512489996", // Good old Nancy Ann Berggren
            "type": {
              /* 
               * The CPR system as identified by the v2 0203 system is NNDNK
               * 
               * If the CPR number is not used, the LR (Local Registry) or
               * "ANON" (Anonymous) could be used instead.
               */
              "coding": [{
                "system": "http://terminology.hl7.org/CodeSystem/v2-0203",
                "code": "NNDNK"
              }]
            }
          }
        ]
        /*
         * Name etc. could be optionally included as well
         */
      }
    },
    
    
    /*
     * QuestionnaireResponse resource
     */
    
    {
      "request": {
        "method": "POST",
        "url": "QuestionnaireResponse"
      },
      "resource": {
        "resourceType": "QuestionnaireResponse",
        "meta": {
          "profile": [
            "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/116129817/QuestionnaireResponse+LinkedMeasurements"
          ]
        },
        "extension": [
          {
            /*
             * The PHG device collecting this measurement
             */
            
            "url": "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/116097025/QuestionnaireResponse+authoringDevice",
            "valueReference": {
              "reference": "7EEDABEE34ADBEEF.C4B301D280C6"
            }
          }
        ],
        "identifier": [{
          
          /* 
           * A QuestionnaireResponse identifier that just has
           * to be a unique string.
           *
           * A QuestionnaireResponse identifier resembling the
           * Observation identifier defined by the H.812.5
           * implementation guide would also work but a UUID works
           * just as well.
           *
           */
            "system": "urn:ietf:rfc:3986",
            "value": "770bc0d0-14a5-430e-a909-321e72f2f2b4"
        }],
        "basedOn": [{
          "identifier": {
            "system": "urn:ietf:rfc:3986",
            "value": "de67e5c1-494a-41b9-924d-4c613b076a73"
          },
          "type": "CarePlan"
        }],
        "language": "da-DK",
        // Canonical link to the questionnaire form
        "questionnaire": "canonical://uuid/Questionnaire/770bc0d0-14a5-430e-a909-321e72f2f2b4|5",
        "status": "completed",
        "subject": {
          "reference": "urn:uuid:d474d2a8-9f94-4836-9094-01108864af54",
          "type": "Patient"
        },
        "authored": "2018-10-09T20:36:42+02:00",
        "author": {
          "reference": "urn:uuid:d474d2a8-9f94-4836-9094-01108864af54",
          "type": "Patient"
        },
        "source": {
          "reference": "urn:uuid:d474d2a8-9f94-4836-9094-01108864af54",
          "type": "Patient"
        },
        "item": [
          
          /*
           * The first group represents a weight measurement
           */
          {
            "linkId": "group1",
            "text": "Din vægt",
            "item": [{
              
              /*
               * Weight question
               */
              
              // We need the id for cross links from Observations
              "id": "anchor1",
              "linkId": "observation1",
              "text": "Vej dig på en badevægt og indtast din vægt her",
              "answer": [{
                "valueQuantity": {
                  "value": 82.0,
                  "unit": "kg",
                  "system": "http://unitsofmeasure.org",
                  "code": "kg"
                }
              }],
              "extension": [
                {
                  /*
                   * The measurement was typed in manually
                   */
                  
                  "url": "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/115736577/QuestionnaireResponse+dataEntryMethod",
                  "valueCoding": {
                    "code": "manual",
                    "system": "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100728837/ObservationDefinition+dataEntryMethod+coding+system",
                    "display": "Manuel indtastning"
                  }
                }, {
                  
                  /*
                   * The observation - weight measurement
                   *
                   * Link to the Observation
                   */
                  
                  "url": "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/116097031/QuestionnaireResponse+linkedObservation",
                  "valueUri":
                    "urn:uuid:81b3797b-93dd-44eb-a564-9fd42aa447a9"
                }
              ]
            }]
          },
          
          /*
           * The second group represents a blood pressure measurement
           */
          
          {
            "linkId": "group2",
            "text": "Dit blodtryk",
            "item": [
              {
                
                /*
                 * Systolic blood pressure question
                 */
                
                "id": "anchor2",
                "linkId": "observation2",
                "text": "Indtast det systoliske blodtryk",
                "answer": [{
                  "valueQuantity": {
                    "value": 122.0,
                    "unit": "mmHg",
                    "system": "http://unitsofmeasure.org",
                    "code": "mm[Hg]"
                  }
                }],
                
                
                "extension": [
                  {
                    /*
                     * The measurement was captured automatically
                     */
                    
                    "url": "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/115736577/QuestionnaireResponse+dataEntryMethod",
                    "valueCoding": {
                      "code": "automatic",
                      "system": "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100728837/ObservationDefinition+dataEntryMethod+coding+system",
                      "display": "Automatisk måling"
                    }
                  }, {
                    
                    /*
                     * The observation - systolic BP measurement
                     *
                     * Link to the Observation
                     */
                    
                    "url": "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/116097031/QuestionnaireResponse+linkedObservation",
                    "valueUri":
                      "urn:uuid:58813e57-f9a1-4153-ab86-6b700b90f44c#anchor1"
                  }
                ]
                
              }, {
                
                /*
                 * Diastolic blood pressure question
                 */
                
                "id": "anchor3",
                "linkId": "observation3",
                "text": "Indtast det diastoliske blodtryk",
                "answer": [{
                  "valueQuantity": {
                    "value": 85.0,
                    "unit": "mmHg",
                    "system": "http://unitsofmeasure.org",
                    "code": "mm[Hg]"
                  }
                }],
                
                
                "extension": [
                  {
                    /*
                     * The measurement was captured automatically
                     */
                    
                    "url": "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/115736577/QuestionnaireResponse+dataEntryMethod",
                    "valueCoding": {
                      "code": "automatic",
                      "system": "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100728837/ObservationDefinition+dataEntryMethod+coding+system",
                      "display": "Automatisk måling"
                    }
                  }, {
                    
                    /*
                     * The observation - diastolic BP measurement
                     *
                     * Link to the Observation
                     */
                    
                    "url": "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/116097031/QuestionnaireResponse+linkedObservation",
                    "valueUri":
                      "urn:uuid:58813e57-f9a1-4153-ab86-6b700b90f44c#anchor2"
                  }
                ]
                
              },{
                
                /*
                 * Model number question
                 */
                
                "linkId": "observation4",
                "text": "Indtast din blodtryksmålers modelbetegnelse",
                "answer": [{
                  "valueString": "UA-767PBT-C"
                }],
                "extension": [
                  {
                    /*
                     * The measurement was captured automatically
                     */
                    
                    "url": "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/115736577/QuestionnaireResponse+dataEntryMethod",
                    "valueCoding": {
                      "code": "automatic",
                      "system": "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100728837/ObservationDefinition+dataEntryMethod+coding+system",
                      "display": "Automatisk måling"
                    }
                  }, {
                    
                    /*
                     * The observation - model number
                     *
                     * Link to the PHD device property
                     */
                    
                    "url": "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/116097031/QuestionnaireResponse+linkedObservation",
                    "valueUri":
                      "Device/00091FFEFF801A33.00091F801A33#anchor1"
                  }
                ]
              }
            ]
          }
        ]
      }
    },
    
    
    
    /*
     * Weight measurement
     * 
     * H.812.5 §8.3.2.3: When an Observation Resource is uploaded,
     * either as a single resource or in a bundle, the PHG shall use
     * the create or conditional create operations.
     * 
     * And in §8.3.2.3.1: a PHG shall use a conditional create to
     * upload the measurement unless one or more of the following
     * conditions are true, in which case a create is used: [No
     * timestamp, or live measurement]
     *
     * The identifier is constructed according to H.812.5. In this
     * case there is no PHD Device, so we use the PHG's MAC address
     * instead (the measurement was typed in here anyway)
     */
    
    {
      "request": {
        "method": "POST",
        "url": "Observation",
        "ifNoneExist": "identifier=2512489996-urn:oid:1.2.208.176.1.2-7E-ED-AB-EE-34-AD-BE-EF-188736-82.0-20181009T183002.00"
      },
      "fullUrl": "urn:uuid:81b3797b-93dd-44eb-a564-9fd42aa447a9",
      "resource": {
        "resourceType": "Observation",
        "meta": {
          "profile": [
            "http://hl7.org/fhir/uv/phd/StructureDefinition/PhdNumericObservation",
            "http://hl7.org/fhir/StructureDefinition/bodyweight",
            "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/116064293/Observation+LinkedMeasurements"
          ]
        },
        "identifier": [
          {
            "value": "2512489996-urn:oid:1.2.208.176.1.2-7E-ED-AB-EE-34-AD-BE-EF-188736-82.0-20181009T183002.00"
          }
        ],
        "basedOn": [{
          "identifier": {
            "system": "urn:ietf:rfc:3986",
            "value": "de67e5c1-494a-41b9-924d-4c613b076a73"
          },
          "type": "CarePlan"
        }],
        "status": "final",
        "code": {
          "coding": [
            {
	      "system" : "urn:oid:1.2.208.176.2.1",
	      "code" : "NPU03804",
	      "display" : "Legeme masse; Pt"
            },{
              "system": "urn:iso:std:iso:11073:10101",
              "code": "188736",
              "display": "MDC_MASS_BODY_ACTUAL"
            },{
              "system": "http://loinc.org",
              "code": "29463-7",
              "display": "Body weight"
            }
          ],
          "text": "MDC_MASS_BODY_ACTUAL: Body weight"
        },
        "category": [{
          "coding": [{
            "system": "http://terminology.hl7.org/CodeSystem/observation-category",
            "code": "vital-signs"
          }]
        }], 
        "subject": {
          "reference": "urn:uuid:d474d2a8-9f94-4836-9094-01108864af54",
          "type": "Patient"
        },
        
        /* 
         * Notice that the performer is not a required element. In a
         * telemonitoring scenario, if a performer is not included, it
         * is implied that the subject is also the performer.
         */
        
        "performer": [
          {
            "reference": "urn:uuid:d474d2a8-9f94-4836-9094-01108864af54",
            "type": "Patient"
          }
        ],
        "effectiveDateTime": "2018-10-09T20:30:02+02:00",
        "valueQuantity": {
          "value": 82.0,
          "system": "http://unitsofmeasure.org",
          "code": "kg",
          "unit": "kg"
        },
        
        /*
         * NOTE:
         * In this case, the PHG is the device responsible for the
         * measurement (as the weight scale is a non-connected
         * device). We do not use the observation-gatewayDevice in
         * this case. 
         *
         * FIXME: Should we?? This must be discussed!
         */
        
        "device": {
          "reference": "7EEDABEE34ADBEEF.C4B301D280C6"
        },
        "extension": [
          {
            /*
             * The measurement was typed in manually
             */
            
            "url": "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/114262030/Observation+dataEntryMethod",
            "valueCoding": {
              "code": "manual",
              "system": "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100728837/ObservationDefinition+dataEntryMethod+coding+system",
              "display": "Manuel indtastning"
            }
          }
        ]
      }
    },
    
    
    
    /*
     * Blood pressure measurement
     */
    
    {
      "request": {
        "method": "POST",
        "url": "Observation",
        "ifNoneExist": "identifier=2512489996-urn:oid:1.2.208.176.1.2-00-09-1F-FE-FF-80-1A-33-150020-122.0-85.0-96.0-20181009T183536.00"
      },
      "fullUrl": "urn:uuid:58813e57-f9a1-4153-ab86-6b700b90f44c",
      "resource": {
        "resourceType": "Observation",
        "meta": {
          "profile": [
            "http://hl7.org/fhir/uv/phd/StructureDefinition/PhdCompoundNumericObservation",
            "http://hl7.org/fhir/StructureDefinition/bp",
            "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/116064293/Observation+LinkedMeasurements"
          ]
        },
        "identifier": [
          {
            "value": "2512489996-urn:oid:1.2.208.176.1.2-00-09-1F-FE-FF-80-1A-33-150020-122.0-85.0-96.0-20181009T183536.00"
          }
        ],
        "basedOn": [{
          "identifier": {
            "system": "urn:ietf:rfc:3986",
            "value": "de67e5c1-494a-41b9-924d-4c613b076a73"
          },
          "type": "CarePlan"
        }],
        "status": "final",
        "code": {
          "coding": [
            {
              "system": "urn:iso:std:iso:11073:10101",
              "code": "150020",
              "display": "MDC_PRESS_BLD_NONINV"
            },{
              "system": "http://loinc.org",
              "code": "85354-9",
              "display": "Blood pressure panel"
            }
          ],
          "text": "MDC_PRESS_BLD_NONINV: Blood pressure"
        },
        "category": [{
          "coding": [{
            "system": "http://terminology.hl7.org/CodeSystem/observation-category",
            "code": "vital-signs"
          }]
        }], 
        "subject": {
          "reference": "urn:uuid:d474d2a8-9f94-4836-9094-01108864af54",
          "type": "Patient"
        },
        "performer": [
          {
            "reference": "urn:uuid:d474d2a8-9f94-4836-9094-01108864af54",
            "type": "Patient"
          }
        ],
        "effectiveDateTime": "2018-10-09T20:35:36+02:00",
        "component": [
          {
            
            /*
             * Systolic component
             */
            
            "code": {
              "coding": [
                {
	          "system" : "urn:oid:1.2.208.176.2.1",
	          "code" : "MCS88019",
	          "display" : "Blodtryk hjemme systolisk;Arm"
	        },{
                  "system": "urn:iso:std:iso:11073:10101",
                  "code": "150021",
                  "display": "MDC_PRESS_BLD_NONINV_SYS"
                },{
                  "system": "http://loinc.org",
                  "code": "8480-6",
                  "display": "Systolic blood pressure"
                }
              ]
            },
            "valueQuantity": {
              "value": 122.0,
              "system": "http://unitsofmeasure.org",
              "code": "mm[Hg]",
              "unit": "mmHg"
            }
          },{
            
            /*
             * Diastolic component
             */
            
            "code": {
              "coding": [
                {
	          "system" : "urn:oid:1.2.208.176.2.1",
	          "code" : "MCS88020",
	          "display" : "Blodtryk hjemme diastolisk;Arm"
	        },{
                  "system": "urn:iso:std:iso:11073:10101",
                  "code": "150022",
                  "display": "MDC_PRESS_BLD_NONINV_DIA"
                },{
                  "system": "http://loinc.org",
                  "code": "8462-4",
                  "display": "Diastolic blood pressure"
                }
              ]
            },
            "valueQuantity": {
              "value": 85.0,
              "system": "http://unitsofmeasure.org",
              "code": "mm[Hg]",
              "unit": "mmHg"
            }
          },{
            
            /*
             * Mean BP component
             */
            
            "code": {
              "coding": [
                {
                  "system": "urn:iso:std:iso:11073:10101",
                  "code": "150023",
                  "display": "MDC_PRESS_BLD_NONINV_MEAN"
                },
                {
                  "system": "http://loinc.org",
                  "code": "8478-0",
                  "display": "Mean blood pressure"
                }
              ]
            },
            "valueQuantity": {
              "value": 96.0,
              "system": "http://unitsofmeasure.org",
              "code": "mm[Hg]",
              "unit": "mmHg"
            }
          }
        ],
        "device": {
          "reference": "00091FFEFF801A33.00091F801A33"
        },
        "extension": [
          {
            /*
             * The PHG device collecting this measurement
             */
            
            "url": "http://hl7.org/fhir/StructureDefinition/observation-gatewayDevice",
            "valueReference": {
              "reference": "7EEDABEE34ADBEEF.C4B301D280C6"
            }
            
          }, {
            /*
             * The measurement was performed automatically
             */
            
            "url": "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/114262030/Observation+dataEntryMethod",
            "valueCoding": {
              "code": "automatic",
              "system": "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100728837/ObservationDefinition+dataEntryMethod+coding+system",
              "display": "Automatisk måling"
            }
          }
        ]
      }
    },
    
    /*
     * Heart rate measurement (performed as part of BP measurement)
     */
    
    {
      "request": {
        "method": "POST",
        "url": "Observation",
        "ifNoneExist": "identifier=2512489996-urn:oid:1.2.208.176.1.2-00-09-1F-FE-FF-80-1A-33-149546-72.0-20181009T183536.00"
      },
      "fullUrl": "urn:uuid:4305004e-3bf6-4edf-abd1-f1ddc2fa52bb",
      "resource": {
        "resourceType": "Observation",
        "meta": {
          "profile": [
            "http://hl7.org/fhir/uv/phd/StructureDefinition/PhdNumericObservation",
            "http://hl7.org/fhir/StructureDefinition/heartrate"
          ]
        },
        "identifier": [
          {
            "value": "2512489996-urn:oid:1.2.208.176.1.2-00-09-1F-FE-FF-80-1A-33-149546-72.0-20181009T183536.00"
          }
        ],
        "basedOn": [{
          "identifier": {
            "system": "urn:ietf:rfc:3986",
            "value": "de67e5c1-494a-41b9-924d-4c613b076a73"
          },
          "type": "CarePlan"
        }],
        "status": "final",
        "code": {
          "coding": [
            {
              "system" : "urn:oid:1.2.208.176.2.1",
              "code" : "NPU21692",
              "display" : "Puls;Hjerte"
            },{
              "system": "urn:iso:std:iso:11073:10101",
              "code": "149546",
              "display": "MDC_PULS_RATE_NON_INV"
            },{
              "system": "http://loinc.org",
              "code": "8867-4",
              "display": "Heart rate"
            }
          ],
          "text": "MDC_PULS_RATE_NON_INV: Heart rate"
        },
        "category": [{
          "coding": [{
            "system": "http://terminology.hl7.org/CodeSystem/observation-category",
            "code": "vital-signs"
          }]
        }],
        "subject": {
          "reference": "urn:uuid:d474d2a8-9f94-4836-9094-01108864af54",
          "type": "Patient"
        },
        "performer": [
          {
            "reference": "urn:uuid:d474d2a8-9f94-4836-9094-01108864af54",
            "type": "Patient"
          }
        ],
        "effectiveDateTime": "2018-10-09T20:35:36+02:00",
        "valueQuantity": {
          "value": 72.0,
          "system": "http://unitsofmeasure.org",
          "code": "/min",
          "unit": "bpm"
        },
        "device": {
          "reference": "00091FFEFF801A33.00091F801A33"
        },
        "extension": [
          {
            /*
             * The PHG device collecting this measurement
             */
            
            "url": "http://hl7.org/fhir/StructureDefinition/observation-gatewayDevice",
            "valueReference": {
              "reference": "7EEDABEE34ADBEEF.C4B301D280C6"
            }
            
          }, {
            /*
             * The measurement was performed automatically
             */
            
            "url": "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/114262030/Observation+dataEntryMethod",
            "valueCoding": {
              "code": "automatic",
              "system": "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100728837/ObservationDefinition+dataEntryMethod+coding+system",
              "display": "Automatisk måling"
            }
          }
        ]
      }
    }
  ]
}
